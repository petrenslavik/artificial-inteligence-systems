﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab1
{
    public partial class Form1 : Form
    {
        public const double precision = 0.0001;
        public Form1()
        {
            InitializeComponent();
        }

        double gauss(double x, double a, double b)
        {
            if (x - a <= precision)
            {
                return 0;
            }

            if (a - x < precision && x - (a + b) / 2 <= precision)
            {
                return 2 * Math.Pow((x - a) / (b - a), 2);
            }

            if ((a + b) / 2 - x < precision && x - b < precision)
            {
                return 1 - 2 * Math.Pow((x - b) / (b - a), 2);
            }

            return 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Series s1 = chart1.Series[0];
            s1.Points.Clear();
            s1.ChartType = SeriesChartType.Spline;
            s1.Name = "Gaussian123";

            double a = Convert.ToDouble(textBox2.Text);
            double b = Convert.ToDouble(textBox1.Text);
            double c = Convert.ToDouble(textBox3.Text);
            double min = 0;
            double max = 5;

            for (double x = min; x <= max; x += (max - min) / 100)
            {
                if (x < b)
                {
                    s1.Points.AddXY(x, gauss(x, a, b));
                }
                else if (b <= x && x <= c)
                {
                    s1.Points.AddXY(x, 1);
                }
                else
                {
                    s1.Points.AddXY(x, 1 - gauss(x, c, c + b - a));
                }
            }
        }
    }
}
