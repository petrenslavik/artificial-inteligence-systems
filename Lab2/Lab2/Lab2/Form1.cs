﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            chart1.Series.Add("Середні чайові");
            chart1.Series.Add("Великі чайові");
            Series s1 = chart1.Series[0];
            Series s2 = chart1.Series[1];
            Series s3 = chart1.Series[2];
            s1.ChartType = SeriesChartType.Spline;
            s2.ChartType = SeriesChartType.Spline;
            s3.ChartType = SeriesChartType.Spline;

            s1.Name = "Малі чайові";

            for (double x = 0.0003; x <= 0.3; x += 0.30 / 1000)
            {
                s1.Points.AddXY(x, triangle(x, 0, 0.1, 0.05));
                s2.Points.AddXY(x, triangle(x, 0.1, 0.2, 0.15));
                s3.Points.AddXY(x, triangle(x, 0.2, 0.3, 0.25));
            }
        }

        public double triangle(double x, double a, double b, double c)
        {
            if (x <= a)
            {
                return 0;
            }
            else if (a < x && x <= c)
            {
                return (x - a) / (c - a);
            }
            else if (c < x && x < b)
            {
                return (b - x) / (b - c);
            }
            else if (b >= x)
            {
                return 0;
            }
            return 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double service = Decimal.ToDouble(numericUpDown1.Value);
            double food = Decimal.ToDouble(numericUpDown1.Value);
            double tip;

            double servRatio = 0.8;
            double lowTip = 0.05;
            double averTip = 0.15;
            double highTip = 0.25;
            double tipRange = highTip - lowTip;
            double badService = 0;
            double okayService = 3;
            double goodService = 7;
            double greatService = 10;
            double serviceRange = greatService - badService;
            double badFood = 0;
            double greatFood = 10;
            double foodRange = greatFood - badFood;

            if (service < okayService)
                tip = (((averTip - lowTip) / (okayService - badService)) * service + lowTip) * servRatio + (1 - servRatio) * (tipRange / foodRange * food + lowTip);

            else if (service < goodService)
                tip = averTip * servRatio + (1 - servRatio) * (tipRange / foodRange * food + lowTip);

            else
                tip = (((highTip - averTip) / (greatService - goodService)) * (service - goodService) + averTip) * servRatio + (1 - servRatio) * (tipRange / foodRange * food + lowTip);
            tipsLabel.Text = "Чайові: " + (Decimal.ToDouble(numericUpDown3.Value) * tip).ToString();
        }
    }
}
